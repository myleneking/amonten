package AmonTen::Model::Log;
use strict;
use warnings;
use File::ReadBackwards;
use IO::File;
use Carp;
use Moose;

has 'log' => (
    is  => 'rw',
    isa => 'Str',
);

has 'rx' => (
    is  => 'ro',
    isa => 'ArrayRef',
);

sub search {
    my ( $self, $opts ) = @_;
    my ( $io, $read_method );

    if ( $opts->{order_by} eq 'backwards' ) {
        $io          = File::ReadBackwards->new( $self->log );
        $read_method = "readline";
    }
    else {
        $io = IO::File->new( $self->log, "r" );
        $read_method = "getline";
    }
    $io || croak "Couldn't open '", $self->log, "' for reading: $!";

    my @match;
    my @items = @{ $self->rx };

  LINE:
    while ( my $line = $io->$read_method ) {
        last LINE if $opts->{rows} and @match >= $opts->{rows};

        my $mline = {};
        foreach my $item (@items) {
            my ( $regex, $desc ) = @$item;

            # Get match string.
            ( $mline->{$desc} ) = $line =~ m/^$regex/;

            # Strip off match string.
            $line =~ s/^$regex//;
        }
        push @match, $mline;
    }

    return wantarray ? @match : \@match;
}

1
