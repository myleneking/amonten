+{  access_key_id     => $ENV{AWS_ACCESS_KEY_ID} || '',
    access_key_secret => $ENV{AWS_SECRET_KEY}    || '',
};
